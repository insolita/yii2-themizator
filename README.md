Themizator - behavior for switch theme
=============

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist insolita/yii2-themizator "*"
```

or add

```
"insolita/yii2-themizator": "*"
```

to the require section of your `composer.json` file.


Usage
-----

In application config

```php
<?php
 return [
     'id' => 'appId',
     'modules'=>[],
     'components'=>[],
     'params'=>[],
     'as themizator'=>[
            'themes'=>[
                'themename1'=>[
                       'pathMap'=>[
                                    '@app/views'=>'@app/themes/theme1/views',
                                    '@app/modules'=>'@app/themes/theme1/modules'
                                  ]
                       ],
                   'themename2'=>[
                                        'pathMap'=>[
                                                     '@app/views'=>'@app/themes/theme2/views',
                                                   ]
                                        ],
                    'bootstrap'=>[
                                        'basePath'=>'@app/themes/bootstrap'
                                    ],
            ],
            'defaultTheme'=>'bootstrap',
            'themeChooser'=>function(){
               //custom theme logic - example
               if(in_array(Yii::$app->controller->module->id,['modulename1','module2',...]){
                   return 'themename1';
               }
               if(in_array(Yii::$app->controller->id,['admin','staff',...]){
                                  return 'themename2';
                }

                $cookies = Yii::$app->request->cookies;
                return $cookies->getValue('apptheme', 'bootstrap');
            }
       ]
     ]
```

So you can direct set theme in action or in controller/module beforeAction() method
```php
\Yii::$app->setCurrentTheme('newtheme2');
```

And dynamic append or overwrite theme configuration by

```php
\Yii::$app->registerTheme('adminlte',['pathMap'=>['@app/views'=>'@app/themes/adminlte/views']]);
```