<?php
/**
 * Created by PhpStorm.
 * User: Insolita
 * Date: 06.04.15
 * Time: 0:13
 */

namespace insolita\themizator;


use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Class Themizator
 *
 * @package insolita\themizator
 */
class Themizator extends Behavior
{

    /**
     * @var array $themes - array with all available themes in format themename=>[themeConfig]
     *                    'themes'=>[
     *                        'theme1name'=>['pathMap'=>['@app/widgets' => '@app/themes/theme1/widgets','@app/views' => '@app/themes/theme1/views']],
     *                        'theme2name'=>['baseUrl'=>'@web/theme2',
     *                                        'pathMap'=>['@app/widgets' => '@app/themes/theme2/widgets','@app/views' =>'@app/themes/theme2/views']],
     * ]
     **/
    public $themes = [];
    /**
     * @var string $currentTheme - current theme name, described in themes array
     **/
    public $currentTheme = false;
    /**
     * @var string $defaultTheme - default theme name, described in themes array
     **/
    public $defaultTheme = 'default';
    /**
     * @var callable|\Closure $themeLoader - function for load $themes property must return array in format themename=>[themeConfig]
     **/
    public $themeLoader = false;
    /**
     * @var callable|\Closure $themeChooser - function for setTheme logic must return themename or false for use default
     **/
    public $themeChooser = false;

    public $themeClass = '\yii\base\Theme';

    /**
     * @return array
     */
    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'theming'
        ];
    }

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if(is_callable($this->themeLoader)){
           $this->themes=call_user_func($this->themeLoader);
        }

        if (empty($this->themes) || empty($this->defaultTheme)
            || !in_array(
                $this->defaultTheme, array_keys($this->themes)
            )
        ) {
            throw new InvalidConfigException('Wrong theme config in Themizator Behavior');
        }
    }


    /**
     * @throws InvalidConfigException
     */
    public function theming()
    {
        if (!$this->currentTheme) {
            $this->currentTheme = ($this->themeChooser) ? call_user_func($this->themeChooser) : false;
            if (!$this->currentTheme) {
                $this->currentTheme = $this->defaultTheme;
            }
        }
        if(!isset($this->themes[$this->currentTheme])){
            $this->currentTheme = $this->defaultTheme;
        }
        $this->owner->controller->getView()->theme = \Yii::createObject(
            ArrayHelper::merge(
                ['class' => $this->themeClass],
                ArrayHelper::getValue($this->themes, $this->currentTheme)
            )
        );
    }

    /**
     * @param string $theme  - name of theme in $themes
     *
     * @throws InvalidConfigException
     */
    public function setCurrentTheme($theme)
    {
        $this->currentTheme = $theme;
        if(!isset($this->themes[$this->currentTheme])){
            throw new InvalidConfigException($this->currentTheme.' - theme not registered');
        }
        $this->owner->controller->getView()->theme = \Yii::createObject(
            ArrayHelper::merge(
                ['class' => $this->themeClass],
                ArrayHelper::getValue($this->themes, $this->currentTheme)
            )
        );
    }

    /**
     * Dynamic add theme (in module usage)
     * @param  string $name
     * @param array $config
     */
    public function registerTheme($name, array $config){
        $this->themes[$name]=$config;
    }
}